

up:
	docker-compose up -d

down:
	docker-compose down

failover:
	docker kill pgha_postgresql-master_1
	docker exec -it pgha_postgresql-slave_1 pg_ctl promote -D /bitnami/postgresql/data
