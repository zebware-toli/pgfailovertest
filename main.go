package main

import (
	"database/sql"
	"log"
	"sync"
	"time"

	_ "github.com/jackc/pgx/v5/stdlib"
)

func main() {
	dsn := "postgres://repl_user:repl_password@127.0.0.11:15001,127.0.0.12:15002/postgres?target_session_attrs=read-write&sslmode=disable"
	pool, err := sql.Open("pgx", dsn)
	if err != nil {
		log.Fatal("unable to use data source name", err)
	}
	defer pool.Close()

	pool.SetConnMaxLifetime(0)
	pool.SetMaxIdleConns(50)
	pool.SetMaxOpenConns(0)

	var mu sync.Mutex
	ok := 0
	fail := 0

	for {
		time.Sleep(time.Second)
		var wg sync.WaitGroup
		var lastErr error

		for i := 0; i < 30; i++ {
			wg.Add(1)
			go func() {
				defer wg.Done()
				var num int
				err := pool.QueryRow("select 42").Scan(&num)
				mu.Lock()
				if err != nil {
					//log.Printf("QueryRow failed: %v", err)
					lastErr = err
					fail++
				} else {
					ok++
				}
				mu.Unlock()
			}()
		}

		wg.Wait()
		log.Printf("ok=%d fail=%d err=%v", ok, fail, lastErr)
	}
}
